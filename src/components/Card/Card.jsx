// import { Component } from "react";
// import styles from "./Cardd.module.css";

// class Card extends Component {
//   componentDidMount() {
//     document.body.style.backgroundColor = "salmon";
//   }

//   componentWillReceiveProps(nextProps) {
//     console.log(nextProps);
//   }

//   componentWillUnmount() {
//     document.body.style.backgroundColor = "white";
//   }

//   render() {
//     return (
//       <>
//         <section className={`App ${styles.card}`}>
//           <h1>Raihan</h1>
//           <button onClick={() => this.setState({ isWork: true })}>
//             Ubah Status Pekerjaan
//           </button>
//         </section>
//       </>
//     );
//   }
// }

// export default Card;

import { useEffect, useState } from "react";

const Card = () => {
  const [count, setCount] = useState(0);
  useEffect(() => {
    setTimeout(() => {
      setCount(10);
    }, 2000);
    console.log("Muncul");
  }, [count]);
  return <h1>Card</h1>;
};

export default Card;
