import { useState } from "react";
const Login = () => {
  const [image, setImage] = useState();
  const handleChangeFile = (dataPhoto) => {
    const file = dataPhoto.target.files[0];
    const reader = new FileReader();
    reader.addEventListener(
      "load",
      () => {
        console.log(reader.result);
        setImage(reader.result);
      },
      false
    );
    reader.readAsDataURL(file);
  };

  return (
    <>
      <h1>Halaman Login</h1>
      <form>
        <img src={image} alt="Preview" />
        <input type="file" onChange={(e) => handleChangeFile(e)} />
      </form>
    </>
  );
};

export default Login;
