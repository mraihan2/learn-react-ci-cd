import { useEffect, useState } from "react";
import axios from "axios";
const DetailProduct = () => {
  const [isOke, setIsOke] = useState();
  const getData = async () => {
    const { data } = await axios(
      "https://rent-cars-api.herokuapp.com/admin/car"
    );
    console.log(data);
  };

  useEffect(() => {
    getData();
    setTimeout(() => {
      setIsOke("isOke");
    }, 2000);
  }, [isOke]);

  return (
    <>
      <p>Detail Product Page</p>
    </>
  );
};

export default DetailProduct;
