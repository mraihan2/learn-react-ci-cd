import { Component } from "react";
import axios from "axios";
import { Outlet } from "react-router-dom";
class Product extends Component {
  constructor() {
    super();
    this.state = {
      isUpdate: false,
    };
  }
  async getData() {
    const { data } = await axios(
      "https://rent-cars-api.herokuapp.com/admin/car"
    );
    console.log(data);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(prevProps, prevState);
    this.getData();
  }

  componentDidMount() {
    this.getData();
    setTimeout(() => {
      this.setState({ isUpdate: true });
    }, 2000);
  }

  render() {
    return (
      <>
        <h1>Halaman Product</h1>
        <Outlet />
      </>
    );
  }
}

export default Product;
