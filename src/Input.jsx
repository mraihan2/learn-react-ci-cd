import { Component } from "react";

class Input extends Component {
  constructor() {
    super();
    this.state = {
      kepo: "kepo",
    };
  }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
    alert("H");
  }
  shouldComponentUpdate() {
    return true;
  }
  componentWillUpdate() {
    // alert("H");
  }
  componentWillUnmount() {
    console.log("Mau ngapain?");
  }
  render() {
    return (
      <>
        <button onClick={() => this.setState({ kepo: "hmm" })}>Ubah</button>
        <input type={this.props.type} placeholder={this.props.placeholder} />
      </>
    );
  }
}

export default Input;

// const Input = (props) => {
//   // const [stateA,setStateA] = useState();

//   useEffect(() => {})
//   return <input type={props.type} placeholder={props.placeholder} />;
// };

// export default Input;
