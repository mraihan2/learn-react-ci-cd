import { BrowserRouter, Routes, Route } from "react-router-dom";
import Product from "./pages/products/Product";
import DetailProduct from "./pages/products/DetailProduct";
import Login from "./pages/auth/Login";

const RouterApp = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="login" element={<Login />} />
        <Route path="product" element={<Product />}>
          <Route path=":productId" element={<DetailProduct />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default RouterApp;
